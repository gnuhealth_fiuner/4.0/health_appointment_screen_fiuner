from trytond.pool import Pool
from trytond.model import (ModelView, ModelSQL,
                    ModelSingleton, fields, sequence_ordered)
from trytond.modules.company.model import CompanyMultiValueMixin
from trytond.pyson import Eval


class Configuration(
        ModelSingleton, ModelSQL, ModelView, CompanyMultiValueMixin):
    'Appointment Screen - Configuration'
    __name__ = 'gnuhealth.appointment.screen.configuration'

    webhook_url = fields.Char("Webhook url",
        required=True)
    images = fields.Many2Many('gnuhealth.appointment.screen.configuration-image',
                    'configuration', 'image', "Images")

    @staticmethod
    def default_webhook_url():
        return "http://localhost:5000/turnos"


class ScreenImage(sequence_ordered(), ModelSQL, ModelView):
    'Appointment Screen - Configuration - Images'
    __name__ = 'gnuhealth.appointment.screen.image'

    name = fields.Char("Name", required=True)
    image = fields.Many2One('ir.attachment', "Image",
            required=True,
            domain=[
                ('resource', '=', 'gnuhealth.appointment.screen.configuration,1'),
                ])
    image_name = fields.Function(
            fields.Char("Title"),
            "on_change_with_image_name")
    duration = fields.Integer("Duration (seconds)", required=True,
            domain=[('duration', '>', 0)])
    activate = fields.Boolean("Activate")

    @fields.depends('image')
    def on_change_with_image_name(self, name=None):
        if self.image:
            return self.image.name
        return None

    @staticmethod
    def default_duration():
        return 30

    @staticmethod
    def default_activate():
        return True


class ConfigurationScreenImage(sequence_ordered(), ModelSQL):
    'Configuration - ScreenImage'
    __name__ = 'gnuhealth.appointment.screen.configuration-image'

    configuration = fields.Many2One('gnuhealth.appointment.screen.configuration',
                        "Configuration", ondelete="CASCADE", required=True)
    image = fields.Many2One('gnuhealth.appointment.screen.image',
                        "Image", ondelete="CASCADE", required=True)
