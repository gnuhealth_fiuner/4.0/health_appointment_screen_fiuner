#-*- coding: utf-8 -*-
from trytond.model import ModelView, fields
from trytond.pool import Pool, PoolMeta
from trytond.transaction import Transaction
from trytond.pyson import Or, Eval, Not, Bool, Equal

import requests
import json

from datetime import datetime


class AppointmentData(metaclass=PoolMeta):
    'Appointment Data'
    __name__ = 'gnuhealth.appointment'

    calling = fields.Boolean("calling")
    in_atention = fields.Boolean("In atention")
    end = fields.Boolean("End")

    @staticmethod
    def default_calling():
        return False

    @staticmethod
    def default_in_atention():
        return False

    @classmethod
    def __setup__(cls):
        super(AppointmentData, cls).__setup__()
        cls._order.insert(0, ('appointment_date', 'DESC'))
        cls._buttons.update({
            'checked_in': {'invisible': Not(Equal(Eval('state'), 'confirmed'))}
            })
        cls._buttons.update({
            'no_show': {'invisible': Not(Equal(Eval('state'), 'confirmed'))}
            })
        cls._buttons.update({
            'call': {'visible': Not( Equal(Eval('state'), 'confirmed'))}})
        cls._buttons.update({
            'in_atention': {'visible': Not( Equal(Eval('state'), 'confirmed'))}})
        cls._buttons.update({
            'end': {'visible': Not( Equal(Eval('state'), 'confirmed'))}})

    @classmethod
    @ModelView.button
    def call(cls, AppointmentData):
        pool = Pool()
        Appointment = pool.get('gnuhealth.appointment')
        Config = pool.get('gnuhealth.appointment.screen.configuration')
        config = Config(1)
        User = Pool().get('res.user')
        user = User(Transaction().user)

        start = datetime.today().replace(hour=0,minute=0,second=0)
        final = datetime.today().replace(hour=23,minute=59,second=59)

        if AppointmentData[0].healthprof.name.internal_user.id == user.id:
            appointments = Appointment.search([
                        ('appointment_date','>=',start),
                        ('appointment_date','<',final),
                        ('healthprof.id','=',AppointmentData[0].healthprof.id),
                        ('state','=','confirmed')
                        ])

            appointment_calling =  {
                    'patient': AppointmentData[0].patient.name.lastname \
                                + ", " + AppointmentData[0].patient.name.name,
                    'professional': AppointmentData[0].healthprof.name.lastname \
                                + ", " + AppointmentData[0].healthprof.name.name,
                    'specialty': AppointmentData[0].healthprof.main_specialty.specialty.name,
                    'time': AppointmentData[0].appointment_date.strftime('%H:%M'),
                    'state': 'calling'
                    }

            appointments_data = {
                    'appointment':appointment_calling,
                    'institution':AppointmentData[0].institution.name.name
                    }

            webhook_url = config.webhook_url
            r = requests.post(webhook_url, data=json.dumps(appointments_data),
                              headers={'Content-Type':'application/json'})
        else:
            cls.raise_user_error('Cada profesional solo puede llamar a sus pacientes')

    @classmethod
    @ModelView.button
    def in_atention(cls, AppointmentData):
        pool = Pool()
        Appointment = pool.get('gnuhealth.appointment')
        Config = pool.get('gnuhealth.appointment.screen.configuration')
        User = Pool().get('res.user')

        config = Config(1)
        user = User(Transaction().user)

        start = datetime.today().replace(hour=0,minute=0,second=0)
        final = datetime.today().replace(hour=23,minute=59,second=59)
        if AppointmentData[0].healthprof.name.internal_user.id == user.id:
            cls.write(AppointmentData, {'state': 'checked_in'})
            appointments = Appointment.search([
                                                ('appointment_date','>=',start),
                                                ('appointment_date','<',final),
                                                ('healthprof.name.internal_user.id','=',user.id),
                                                ('state','=','confirmed')
                                                ])

            appointment_calling = {'patient':AppointmentData[0].patient.name.lastname+", "+AppointmentData[0].patient.name.name,
                            'professional':AppointmentData[0].healthprof.name.lastname+", "+AppointmentData[0].healthprof.name.name,
                            'specialty': AppointmentData[0].healthprof.main_specialty.specialty.name,
                            'time':AppointmentData[0].appointment_date.strftime('%H:%M'),
                            'state':'in_atention'
                            }

            appointments_data = {   'appointment':appointment_calling,
                                    'institution':AppointmentData[0].institution.name.name
                                    }

            webhook_url = config.webhook_url
            r = requests.post(webhook_url, data=json.dumps(appointments_data), headers={'Content-Type':'application/json'})
        else:
            cls.raise_user_error('Cada profesional solo puede llamar a sus pacientes')

    @classmethod
    @ModelView.button
    def no_show(cls, AppointmentData):
        pool = Pool()
        Config = pool.get('gnuhealth.appointment.screen.configuration')
        config = Config(1)
        User = Pool().get('res.user')
        user = User(Transaction().user)

        start = datetime.today().replace(hour=0,minute=0,second=0)
        final = datetime.today().replace(hour=23,minute=59,second=59)

        cls.write(AppointmentData, {'state': 'no_show'})

        Appointment = pool.get('gnuhealth.appointment')
        appointments = Appointment.search([
                            ('appointment_date','>=',start),
                            ('appointment_date','<',final),
                            ('healthprof.name.internal_user.id','=',user.id),
                            ('state','=','confirmed')
                            ])

        appointment_calling = {'patient':AppointmentData[0].patient.name.lastname+", "+AppointmentData[0].patient.name.name,
                            'professional':AppointmentData[0].healthprof.name.lastname+", "+AppointmentData[0].healthprof.name.name,
                            'specialty': AppointmentData[0].healthprof.main_specialty.specialty.name,
                            'time':AppointmentData[0].appointment_date.strftime('%H:%M'),
                            'state':'no_show'
                            }

        appointments_data = {   'appointment':appointment_calling,
                                    'institution':AppointmentData[0].institution.name.name
                                    }

        webhook_url = config.webhook_url
        r = requests.post(webhook_url, data=json.dumps(appointments_data), headers={'Content-Type':'application/json'})

    @classmethod
    @ModelView.button
    def end(cls, AppointmentData):
        pool = Pool()
        Config = pool.get('gnuhealth.appointment.screen.configuration')
        config = Config(1)
        User = Pool().get('res.user')
        user = User(Transaction().user)

        start = datetime.today().replace(hour=0,minute=0,second=0)
        final = datetime.today().replace(hour=23,minute=59,second=59)

        Appointments = pool.get('gnuhealth.appointment')
        appointments = Appointments.search([
                                                ('appointment_date','>=',start),
                                                ('appointment_date','<',final),
                                                ('healthprof.name.internal_user.id','=',user.id),
                                                ('state','=','confirmed')
                                                ])

        appointment_calling = {'patient':AppointmentData[0].patient.name.lastname+", "+AppointmentData[0].patient.name.name,
                            'professional':AppointmentData[0].healthprof.name.lastname+", "+AppointmentData[0].healthprof.name.name,
                            'specialty': AppointmentData[0].healthprof.main_specialty.specialty.name,
                            'time':AppointmentData[0].appointment_date.strftime('%H:%M'),
                            'state':'end'
                            }

        appointments_data = {   'appointment':appointment_calling,
                                    'institution':AppointmentData[0].institution.name.name
                                    }

        webhook_url = config.webhook_url
        r = requests.post(webhook_url, data=json.dumps(appointments_data),
                          headers={'Content-Type':'application/json'})
